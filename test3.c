#include <stdio.h>
#include <math.h>


int main()
{
	int b, a, c;
	float D;
	printf("The Quadratic Equation is ax^2 + bx + c, Enter the value of a, b and c: ");
	scanf("%d%d%d", &a, &b, &c);
	D = pow(b,2) - 4*a*c;
	
	if(D<0)
		printf("The roots are imaginary ");
	if(D==0)
	{
		float r;
		r = -b/(2*a);
		printf("The roots are equal and %f", r);
	}
	if(D>0)
	{
		float r1, r2;
		r1 = (-b + pow(D, 0.5)/(2*a));
		r2 = (-b - pow(D, 0.5)/(2*a));
		printf("The roots are real and unequal: %f and %f ", r1, r2);
	}
	return 0;
}