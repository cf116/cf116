#include <stdio.h>

int main()
{
	int num=0,sum=0, rem=0;
	printf("Enter a Number: ");
	scanf("%d", &num);
	while(num>0)
	{
        rem = num%10;
		sum+=rem;
        num = num/10;
	}
	printf("The sum of the digits of the number is %d \n", sum);
	return 0;
}