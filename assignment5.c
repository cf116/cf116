#include <stdio.h>

int main()
{
	int n, remainder, rev=0;
	printf("Enter an integer: ");
	scanf("%d", &n);
	while(n!=0)
	{
		remainder=n%10;
		rev = rev * 10 + remainder;
		n/=10;
	}
	
	printf("The reversed integer is %d", rev);
	return 0;
}