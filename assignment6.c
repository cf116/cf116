#include <stdio.h>

int main()
{
    int  prime[10] = {2, 3, 5, 11, 13, 17, 19, 23};
    int position, c, value;
        
    printf("The array at present is: \n");
    for (c = 0; c <= 7; c++)    
    printf("%d ", prime[c]);  
    
    printf("\nPlease enter the location where you want to insert an new element\n");
    scanf("%d", &position);
    
    printf("Please enter the value\n");
    scanf("%d", &value);
    
    for (c = 8; c >= position - 1; c--)    
    prime[c+1] = prime[c];
    
    prime[position-1] = value;
    
    printf("Resultant array is\n");
    
    for (c = 0; c <= 8; c++)    
    printf("%d ", prime[c]);    
    
    return 0;
}