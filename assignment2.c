#include <stdio.h>
int sum ( int num1, int num2, int num3);

int main()
{
	int num1=0, num2=0, num3=0;
	printf("Enter 3 numbers: ");
	scanf("%d%d%d", &num1, &num2, &num3);
	printf("The sum of the 3 numbers is %d", sum(num1, num2, num3));
	return 0;
}

int sum ( int num1, int num2, int num3)
{
	int total=0;
	total = num1 + num2 + num3;
	return total;
}