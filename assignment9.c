#include<stdio.h>

struct student
{
 int rno;
 char name[20];
 char section;
 char dept[20];
 float fees;
 float result;
};

void display(struct student s);

int main()
{
 struct student s1;
 struct student s2;
 printf("enter the details of first student\n");
 printf("enter roll number\n");
 scanf("%d",&s1.rno);
 printf("enter name\n");
 scanf("%s",s1.name);
 printf("enter section\n");
 scanf(" %c",&s1.section);
 printf("enter department\n");
 scanf("%s",s1.dept);
 printf("enter fees\n");
 scanf("%f",&s1.fees);
 printf("enter result\n");
 scanf("%f",&s1.result);
 printf("enter the details of second student\n");
 printf("enter roll number\n");
 scanf("%d",&s2.rno);
 printf("enter name\n");
 scanf("%s",s2.name);
 printf("enter section\n");
 scanf(" %c",&s2.section);
 printf("enter department\n");
 scanf("%s",s2.dept);
 printf("enter fees\n");
 scanf("%f",&s2.fees);
 printf("enter result\n");
 scanf("%f",&s2.result);
 if(s1.result>s2.result)
 display(s1);
 else
 diplay(s2);
 return 0;
}

void display(struct student s)
{
 printf("Roll no=%d",s.rno);
 printf("name=%s",s.name);
 printf("section= %c",s.section);
 printf("department=%s",s.dept);
 printf("fees=%f",s.fees);
 printf("result=%f",s.result);
}