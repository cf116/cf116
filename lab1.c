#include <stdio.h>
#include <math.h>

int main()
{
	float x1, x2, y1, y2, distance;
	printf("Enter the value of y2, y1, x2, x1: ");
	scanf("%f%f%f%f", &y2, &y1, &x2, &x1);
	distance = sqrt( pow((y2-y1), 2)- pow((x2-x1),2) );
	printf("The distance between 2 points is: %f", distance);
	return 0;
}