#include <stdio.h>

int main()
{
	int n, m;
	printf("Enter the starting and ending number: ");
	scanf("%d%d", &n, &m);
	printf("\nNumber and their squares\n");
	while(m>=n)
	{
		int square = n*n;
		printf("%d \t %d \n",n,square); 
		n++;
	}
	return 0;
}