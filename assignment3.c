#include <stdio.h>

int main()
{
	int sub1=0, sub2=0, sub3=0, sub4=0, sub5=0;
	int marks;
    printf("Please enter the marks of the student in 5 subjects : ");
	scanf("%d%d%d%d%d", &sub1, &sub2, &sub3, &sub3, &sub4, &sub5);
	if(sub1>=0&&sub2>=40&&sub3>=40&&sub4>=40&&sub5>=40)
	{	
		marks = (sub1+sub2+sub3+sub4+sub5)/5;
		printf("The average marks is %d \n", marks);
	
		if(marks>=90 && marks<=100)
		printf("The grade is S");
		else if(marks>=80 && marks<=89)
			printf("The grade is A");
		else if(marks>=70 && marks<=79)
			printf("The grade is B");
		else if(marks>=60 && marks<=69)
			printf("The grade is C");
		else if(marks>=50 && marks<=59)
			printf("The grade is D");
		else if(marks>=40 && marks<=49)
			printf("The grade is E");
		else if(marks<40)
			printf("The final grade is F");
	}
	else 
		printf("The Grade is F \n";
    
	return 0;
}