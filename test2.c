#include <stdio.h>

float circle_area (float radius);

int main()
{
	float radius=0;
	printf("Enter the radius of the circle: ");
	scanf("%f", &radius);
	printf("The area of the circle is: %f", circle_area(radius));
	return 0;
}

float circle_area (float radius)
{
	float area =0;
	area=3.14*radius*radius;
	return area;
}