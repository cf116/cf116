#include <stdio.h>

int main()
{   
    int a[6] = {22, 14, 30, 25, 11, 43};
    int flag = 0, position = 0;
    for(int i = 0; i <= 5; i++)
    {
        if(a[i] == 25)
        {
            flag = 1;
            position = i + 1;
            break;
        }
    }
    
    if(flag==1)
    {
        printf("\nNumber Found In an Array \n");
        printf("The position is %d\n", position); 
    }
    else 
    {
        printf("Number Not Found In an Array \n");
    }
    
return 0;
}