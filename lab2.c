#include <stdio.h>
 
int main()
{
	int hours, mins, final_time;
	printf("Enter the time in hours and minutes: ");
	scanf("%d%d", &hours, &mins);
	hours = hours * 60;
	final_time = hours + mins;
	printf("The final time in minutes is: %d", final_time);
	return 0;
}